const initialDate = {
  startDate: new Date(Date.now() - 2 * 24 * 60 * 60 * 1000),
  endDate: new Date(),
};
export const date = (state = initialDate, action) => {
  let draft = state;
  switch (action.type) {
    case "DATE":
      const { startDate, endDate } = action.date;
      draft = {
        startDate,
        endDate,
      };
      return draft;
    default:
      return state;
  }
};
