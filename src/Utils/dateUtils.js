import {
  addDays,
  endOfDay,
  startOfDay,
  startOfMonth,
  endOfMonth,
  addMonths,
  startOfWeek,
  endOfWeek,
  isSameDay,
  differenceInCalendarDays,
  subMonths,
  subYears,
  subDays,
} from "date-fns";

const defineds = {
  startOfWeek: startOfWeek(new Date()),
  endOfWeek: endOfWeek(new Date()),
  startOfLastWeek: startOfWeek(addDays(new Date(), -7)),
  endOfLastWeek: endOfWeek(addDays(new Date(), -7)),
  startOfToday: startOfDay(new Date()),
  endOfToday: endOfDay(new Date()),
  startOfYesterday: startOfDay(addDays(new Date(), -1)),
  endOfYesterday: endOfDay(addDays(new Date(), -1)),
  startOfMonth: startOfMonth(new Date()),
  endOfMonth: endOfMonth(new Date()),
  startOfLastMonth: startOfMonth(addMonths(new Date(), -1)),
  endOfLastMonth: endOfMonth(addMonths(new Date(), -1)),
  threeMonthsAgo: subMonths(new Date(), 3),
  sixMonthsAgo: subMonths(new Date(), 6),
  oneYearAgo: subYears(new Date(), 1),
  twoWeeksAgo: startOfDay(subDays(new Date(), 13)),
};

export const getInputRanges = () => {
  const defaultInputRanges = [
    {
      label: "Days Up To Today",
      range(value) {
        return {
          startDate: addDays(
            defineds.startOfToday,
            (Math.max(Number(value), 1) - 1) * -1
          ),
          endDate: defineds.endOfToday,
        };
      },
      getCurrentValue(range) {
        if (!isSameDay(range.endDate, defineds.endOfToday)) return "-";
        if (!range.startDate) return "∞";
        return (
          differenceInCalendarDays(defineds.endOfToday, range.startDate) + 1
        );
      },
    },
  ];
  return defaultInputRanges;
};

const staticRangeHandler = {
  range: {},
  isSelected(range) {
    const definedRange = this.range();
    return (
      isSameDay(range.startDate, definedRange.startDate) &&
      isSameDay(range.endDate, definedRange.endDate)
    );
  },
};

function createStaticRanges(ranges) {
  return ranges.map((range) => ({ ...staticRangeHandler, ...range }));
}

export const getStaticRanges = () => {
  const ranges = createStaticRanges([
    {
      label: "This Week",
      range: () => ({
        startDate: defineds.startOfWeek,
        endDate: defineds.endOfWeek,
      }),
    },
    {
      label: "Past Two Weeks",
      range: () => ({
        startDate: defineds.twoWeeksAgo,
        endDate: defineds.endOfToday,
      }),
    },
    {
      label: "This Month",
      range: () => ({
        startDate: defineds.startOfMonth,
        endDate: defineds.endOfMonth,
      }),
    },
    {
      label: "Past Three Months",
      range: () => ({
        startDate: defineds.threeMonthsAgo,
        endDate: defineds.endOfToday,
      }),
    },
    {
      label: "Past Six Months",
      range: () => ({
        startDate: defineds.sixMonthsAgo,
        endDate: defineds.endOfToday,
      }),
    },
    {
      label: "Past Year",
      range: () => ({
        startDate: defineds.oneYearAgo,
        endDate: defineds.endOfToday,
      }),
    },
  ]);
  return ranges;
};
