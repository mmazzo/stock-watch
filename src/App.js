import "./App.css";
import Container from "./components/Container/Container";
import { Provider as ReduxProvider } from "react-redux";
import configureStore from "./redux";
const store = configureStore();

function App() {
  return (
    <div>
      <ReduxProvider store={store}>
        <Container />
      </ReduxProvider>
    </div>
  );
}

export default App;
