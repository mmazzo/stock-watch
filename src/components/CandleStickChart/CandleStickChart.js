import React, { Component } from "react";
import PropTypes from "prop-types";
import "./candleStickChart.css";
import ReactApexCharts from "react-apexcharts";

class CandleStickChart extends Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
  }

  getData = () => {
    const { history } = this.props;
    if (history && history.length) {
      return [
        {
          data: [...Array(history.length)].map((_, i) => ({
            x: history[i].date.toISOString(),
            y: [
              history[i].open.toFixed(2),
              history[i].high.toFixed(2),
              history[i].low.toFixed(2),
              history[i].close.toFixed(2),
            ],
          })),
        },
      ];
    }
  };

  render() {
    const series = this.getData();
    const options = {
      title: {
        text: "Candle Stick",
        align: "left",
      },
      xaxis: {
        type: "datetime",
      },
      yaxis: {
        tooltip: {
          enabled: true,
        },
      },
    };
    return (
      <div className="chart">
        <ReactApexCharts
          options={options}
          series={series}
          type="candlestick"
          height={245}
        />
      </div>
    );
  }
}

CandleStickChart.propTypes = {
  history: PropTypes.object,
};

export default CandleStickChart;
