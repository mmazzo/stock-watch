import React, { Component } from "react";

import PropTypes from "prop-types";
import "./chartSlider.css";
import SwiperCore, { Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.scss";
import "swiper/components/navigation/navigation.scss";
import CandleStickChart from "../CandleStickChart/CandleStickChart";
import LineChart from "../LineChart/LineChart";
import ScatterPlotChart from "../ScatterPlotChart/ScatterPlotChart";

class ChartSlider extends Component {
  render() {
    const { history } = this.props;
    SwiperCore.use([Navigation]);

    return (
      <div class="chartSlider">
        <Swiper
          spaceBetween={0}
          slidesPerView={1}
          navigation
          onSlideChange={() => console.log("slide change")}
          onSwiper={(swiper) => console.log(swiper)}
        >
          <SwiperSlide>
            <LineChart history={history} />
          </SwiperSlide>
          <SwiperSlide>
            <CandleStickChart history={history} />
          </SwiperSlide>
          <SwiperSlide>
            <ScatterPlotChart history={history} />
          </SwiperSlide>
        </Swiper>
      </div>
    );
  }
}
ChartSlider.propTypes = {
  history: PropTypes.object,
};

export default ChartSlider;
