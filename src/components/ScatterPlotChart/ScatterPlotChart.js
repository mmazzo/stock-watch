import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactApexCharts from "react-apexcharts";

class ScatterPlotChart extends Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
  }

  getData = () => {
    const { history } = this.props;
    if (history && history.length) {
      return [
        {
          data: [...Array(history.length)].map((_, i) => ({
            x: history[i].date.toISOString(),
            y: history[i].close.toFixed(2),
          })),
        },
      ];
    }
  };

  render() {
    const series = this.getData();
    const options = {
      stroke: {
        width: 3,
      },
      title: {
        text: "Scatter",
        align: "left",
      },
      fill: {
        type: "gradient",
        gradient: {
          shadeIntensity: 1,
          inverseColors: false,
          opacityFrom: 0.5,
          opacityTo: 0.8,
        },
      },
      colors: ["#3e484f"],
      xaxis: {
        type: "datetime",
      },
      yaxis: {
        tooltip: {
          enabled: true,
        },
      },
    };
    return (
      <div className="chart">
        <ReactApexCharts
          options={options}
          series={series}
          type="scatter"
          height={245}
        />
      </div>
    );
  }
}

ScatterPlotChart.propTypes = {
  history: PropTypes.object,
};

export default ScatterPlotChart;
