import React, { Component } from "react";

import StockCard from "../StockCard/StockCard";
import PropTypes from "prop-types";
import "./chartContainer.css";

class ChartContainer extends Component {
  render() {
    const { data } = this.props;
    const stockCards = [];
    data.forEach((value, key) =>
      stockCards.push(
        <StockCard symbol={key} quote={value.quote} history={value.history} />
      )
    );
    return <div class="chartContainer">{stockCards}</div>;
  }
}
ChartContainer.propTypes = {
  stockData: PropTypes.instanceOf(Map).isRequired,
};

export default ChartContainer;
