(This is a work in progress)

Current Functionality:

On the header of the SPA a user can enter a date range and up to 4 stock symbols. For each valid symbol entered a card will appear containing data that's been pulled from the Yahoo Finance API.

![Search Functionality](/public/screenshots/SearchFunctionality.png)

The card will contain summary information, and a slider component to scroll through different charts. The supported charts are: Line, Candle Stick, and Scatter.

![Loaded Stock Data](/public/screenshots/CardsAndCharts.png)

For each card, the summary section is interactable, and will open a drawer containing more information once clicked.

![Current Functionality](/public/screenshots/Details.png)


ToDo:
- Add chart generator instead of separate components for each
- Add a component (above stock cards) to display data from each of the specificed stock symbols in a single chart for comparing
- Jest/Enzyme tests
- Improve error handling
- Add more detailed graphs to each card/slider component
- Add more data to the stock drawer and improve format
- Improve UI