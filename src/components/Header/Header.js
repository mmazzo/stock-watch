import React, { Component } from "react";
import { Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { actions, selectors } from "../../redux";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import flow from "lodash.flow";
import {
  Navbar,
  Button,
  NavbarGroup,
  NavbarHeading,
  NavbarDivider,
  Alignment,
  TagInput,
} from "@blueprintjs/core";
import "./header.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import DateSelector from "../DateSelector/DateSelector";
import { createErrorToast } from "../../Utils/utils";

class Header extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleDateSelection = this.handleDateSelection.bind(this);
    this.state = {
      date: {
        startDate: this.props.date.startDate,
        endDate: this.props.date.endDate,
      },
      symbols: [],
      data: {},
    };
  }

  handleDateSelection(date) {
    const { startDate, endDate } = date;
    this.setState({
      ...this.state,
      date: {
        startDate,
        endDate,
      },
    });
  }

  handleSearch() {
    const { setDate, getSymbolQuote, getSymbolHistory } = this.props;
    const { symbols, date } = this.state;
    if (!symbols || new Date() < date.startDate || new Date() < date.endDate) {
      return;
    } else {
      this.props.handleClearData();
      setDate(date);
      symbols.forEach((symbol) =>
        Promise.all([
          getSymbolQuote(symbol),
          getSymbolHistory(symbol, date),
        ]).then((e) =>
          this.props.handleUpdateData(symbol, { quote: e[0], history: e[1] })
        )
      );
    }
  }
  handleAddTag = (e) => {
    const { symbols } = this.state;
    if (symbols.indexOf(e[0].toUpperCase()) > -1) {
      createErrorToast("Symbol already exists");
    } else if (symbols.length > 3) {
      createErrorToast("Maximum of four symbols");
    } else {
      symbols.push(e[0].toUpperCase());
    }
  };
  handleRemoveTag = (e) => {
    const { symbols } = this.state;
    this.setState({
      symbols: symbols.filter(
        (symbol) => symbol !== symbols[symbols.indexOf(e)]
      ),
    });
  };

  render() {
    const { symbols } = this.state;
    return (
      <Navbar fixedToTop className="nav-bar bp3-navbar bp3-dark">
        <NavbarGroup align={Alignment.LEFT}>
          <NavbarHeading>Stock Watch</NavbarHeading>
          <Icon
            className="header-icon"
            iconSize={13}
            icon={IconNames.TIMELINE_LINE_CHART}
          ></Icon>
          <NavbarDivider />
        </NavbarGroup>
        <NavbarGroup align={Alignment.RIGHT}>
          <DateSelector handleDateSelection={this.handleDateSelection} />
          <TagInput
            leftIcon={IconNames.TRENDING_UP}
            placeholder="Enter Symbols"
            values={symbols}
            addOnBlur
            addOnPaste
            onAdd={this.handleAddTag}
            onRemove={this.handleRemoveTag}
            className="symbolInput"
            tagProps={{ round: true }}
          />
          <Button
            className="bp3-button bp3-minimal"
            icon={IconNames.SEARCH}
            type="submit"
            text=""
            onClick={this.handleSearch}
          />
        </NavbarGroup>
      </Navbar>
    );
  }
}
Header.propTypes = {
  handleUpdateData: PropTypes.func,
  handleClearData: PropTypes.func,
  getSymbolQuote: PropTypes.func,
  getSymbolHistory: PropTypes.func,
  handleDateSelection: PropTypes.func,
  date: PropTypes.shape({
    startDate: PropTypes.instanceOf(Date),
    endDate: PropTypes.instanceOf(Date),
  }),
};

export const mapStateToProps = (state) => ({
  date: selectors.getDate(state),
});

const mapDispatchToProps = {
  setDate: actions.setDate,
};

const enhance = flow(connect(mapStateToProps, mapDispatchToProps));

export default enhance(Header);
