import React, { Component } from "react";
import { H1, H2, H4, H5, Divider } from "@blueprintjs/core";
import { currencyFormatter } from "../../Utils/utils";

import "./stockDrawer.css";
import PropTypes from "prop-types";

class StockDrawer extends Component {
  render() {
    const {
      price,
      summaryProfile,
      summaryDetail,
      financialData,
    } = this.props.quote;
    const formatter = currencyFormatter(summaryDetail.currency);

    return (
      <div class="stockDrawer">
        <H1 class="stockDrawerHeaderLink">
          <a href={summaryProfile.website}>{price.longName}</a>
        </H1>
        <Divider />
        <H2>
          {price.symbol} ({formatter.format(financialData.currentPrice)})
        </H2>
        <H4>{`${summaryProfile.address1}, ${summaryProfile.city}, ${summaryProfile.country} ${summaryProfile.zip}`}</H4>
        <H4>{`${summaryProfile.industry} | ${summaryProfile.sector}`}</H4>
        <H5>{summaryProfile.longBusinessSummary}</H5>
      </div>
    );
  }
}

StockDrawer.propTypes = {
  quote: PropTypes.object,
};

export default StockDrawer;
