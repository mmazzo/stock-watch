import React, { Component } from "react";
import Header from "../Header/Header";
import ChartContainer from "../ChartContainer/ChartContainer";
import { selectors } from "../../redux";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import flow from "lodash.flow";
import { createErrorToast, createSuccessToast } from "../../Utils/utils";

const yahooFinance = require("yahoo-finance");

class Container extends Component {
  constructor(props) {
    super(props);
    this.handleUpdateData = this.handleUpdateData.bind(this);
    this.handleClearData = this.handleClearData.bind(this);
    this.state = {
      data: new Map(),
    };
  }

  handleUpdateData = (symbol, data) => {
    const { quote, history } = data;
    console.log("Updating..");
    this.setState((prevState) => ({
      data: prevState.data.set(symbol, { quote, history }),
    }));
  };
  handleClearData = () => {
    this.setState({ data: new Map() });
  };

  async getSymbolQuote(symbol) {
    const quote = await yahooFinance.quote(
      {
        symbol,
        modules: ["price", "summaryDetail", "summaryProfile", "financialData"],
      },
      (err, quote) => {
        if (err) {
          createErrorToast(`Invalid Symbol: ${symbol.toUpperCase()}`);
          return null;
        } else {
          return quote;
        }
      }
    );
    return quote;
  }
  async getSymbolHistory(symbol, date) {
    const history = await yahooFinance.historical(
      {
        symbol: symbol,
        from: date.startDate,
        to: date.endDate,
        period: "d",
      },
      (err, quotes) => {
        if (quotes.length) {
          createSuccessToast("Successfully Processed Request");
          return quotes;
        } else {
          createErrorToast(`Invalid Symbol: ${symbol.toUpperCase()}`);
          return null;
        }
      }
    );
    return history;
  }

  render() {
    const { data } = this.state;
    return (
      <>
        <Header
          handleUpdateData={this.handleUpdateData}
          getSymbolQuote={this.getSymbolQuote}
          getSymbolHistory={this.getSymbolHistory}
          handleClearData={this.handleClearData}
          handleDateSelection={this.handleDateSelection}
        />
        <ChartContainer data={data} />
      </>
    );
  }
}

Container.propTypes = {
  date: PropTypes.shape({
    startDate: PropTypes.instanceOf(Date),
    endDate: PropTypes.instanceOf(Date),
  }),
};

export const mapStateToProps = (state) => ({
  date: selectors.getDate(state),
});

const enhance = flow(connect(mapStateToProps));
export default enhance(Container);
