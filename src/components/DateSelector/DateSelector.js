import React, { Component } from "react";
import { DateRangePicker } from "react-date-range";
import { getInputRanges, getStaticRanges } from "../../Utils/dateUtils";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import "@blueprintjs/core/lib/css/blueprint.css";

import { Button, Popover, Position } from "@blueprintjs/core";
import { IconContents, IconNames } from "@blueprintjs/icons";
import "./dateSelector.css";
import { selectors } from "../../redux";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import flow from "lodash.flow";

class DateSelector extends Component {
  constructor(props) {
    super(props);
    this.handleDateSelection = this.handleDateSelection.bind(this);
    this.getButtonText = this.getButtonText.bind(this);

    this.state = {
      startDate: this.props.date.startDate,
      endDate: this.props.date.endDate,
    };
  }

  handleDateSelection(e) {
    const { startDate, endDate } = e.selection;
    this.setState({
      startDate,
      endDate,
    });
    this.props.handleDateSelection({ startDate, endDate });
  }

  getButtonText() {
    const { startDate, endDate } = this.state;
    return `${startDate.toLocaleDateString()} ${
      IconContents.ARROW_RIGHT
    } ${endDate.toLocaleDateString()}`;
  }

  render() {
    const { startDate, endDate } = this.state;
    const selectionRange = {
      startDate,
      endDate,
      key: "selection",
    };
    // background-color: #394b59;#202b33
    return (
      <Popover>
        <Button
          className="popoverButton"
          icon={IconNames.CALENDAR}
          position={Position.BOTTOM}
        >
          {this.getButtonText()}
        </Button>
        <DateRangePicker
          ranges={[selectionRange]}
          onChange={this.handleDateSelection}
          showDateDisplay={false}
          rangeColors={["#394b59"]}
          color="#008000"
          shownDate={new Date()}
          inputRanges={getInputRanges()}
          staticRanges={getStaticRanges()}
        />
      </Popover>
    );
  }
}

DateSelector.propTypes = {
  date: PropTypes.shape({
    startDate: PropTypes.instanceOf(Date),
    endDate: PropTypes.instanceOf(Date),
  }),
  handleDateSelection: PropTypes.func,
  onClose: PropTypes.func,
};

export const mapStateToProps = (state) => ({
  date: selectors.getDate(state),
});

const enhance = flow(connect(mapStateToProps));
export default enhance(DateSelector);
