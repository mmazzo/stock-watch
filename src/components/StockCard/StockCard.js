import React, { Component } from "react";
import {
  Card,
  Text,
  Elevation,
  Drawer,
  H2,
  H3,
  H5,
  H6,
  Divider,
} from "@blueprintjs/core";
import { currencyFormatter } from "../../Utils/utils";
import "./stockCard.css";

import PropTypes from "prop-types";
import StockDrawer from "../StockDrawer/StockDrawer";
import ChartSlider from "../ChartSlider/ChartSlider";

class StockCard extends Component {
  constructor(props) {
    super(props);
    this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
    this.state = {
      openDrawer: false,
    };
  }

  handleCloseDrawer = () => {
    this.setState({
      ...this.state,
      openDrawer: false,
    });
  };

  handleOpenDrawer = () => {
    console.log("OPENING..");
    this.setState({
      ...this.state,
      openDrawer: true,
    });
  };
  render() {
    const { symbol, quote, history } = this.props;
    const formatter = currencyFormatter(quote.summaryDetail.currency);

    return (
      <div>
        <Card className="stockCardFull" elevation={Elevation.THREE}>
          <Card
            interactive={true}
            className="stockCardSummary"
            onClick={this.handleOpenDrawer}
          >
            <H2>
              <Text ellipsize>{quote.price.shortName}</Text>
            </H2>
            <Divider />
            <H3>
              {symbol} ({formatter.format(quote.financialData.currentPrice)})
            </H3>
            <div class="fiftyTwoWeek">
              <H5>52 Week</H5>
              <H6>
                High: {formatter.format(quote.summaryDetail.fiftyTwoWeekHigh)}
              </H6>
              <H6>
                Low: {formatter.format(quote.summaryDetail.fiftyTwoWeekLow)}
              </H6>
            </div>
          </Card>
          <ChartSlider history={history} />
        </Card>
        <Drawer isOpen={this.state.openDrawer} onClose={this.handleCloseDrawer}>
          <StockDrawer quote={quote} />
        </Drawer>
      </div>
    );
  }
}
StockCard.propTypes = {
  symbol: PropTypes.string,
  quote: PropTypes.object,
  history: PropTypes.object,
};

export default StockCard;
