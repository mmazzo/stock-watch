import { Toaster, Intent, Position } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import "@blueprintjs/core/lib/css/blueprint.css";

export const createErrorToast = (message) => {
  const toast = Toaster.create({
    position: Position.TOP,
  });
  toast.show({ message, intent: Intent.DANGER, icon: IconNames.WARNING_SIGN });
};

export const createSuccessToast = (message) => {
  const toast = Toaster.create({
    position: Position.TOP,
  });
  toast.show({ message, intent: Intent.SUCCESS, icon: IconNames.TICK_CIRCLE });
};

export const currencyFormatter = (currency) => {
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency,
  });
};
